https://gitlab.com/KopylovMikhail/jse-08

<b>SOFTWARE REQUIREMENTS:</b>
 - JRE 1.8
 
<b>TECHNOLOGY STACK:</b>
  - IntelliJ IDEA,
  - JAVA 8,
  - Maven 4.0
  
<b>DEVELOPER:</b>

 Mikhail Kopylov
<br> e-mail: kopylov_m88@mail.ru

<b>BUILD COMMANDS:</b>
```
mvn clean
```
```
mvn install
```

<b>RUN FROM CONSOLE:</b>
```
java -jar <PROJECT_FOLDER>\target\release\bin\taskmanager.jar
```