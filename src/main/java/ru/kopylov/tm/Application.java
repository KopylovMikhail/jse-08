package ru.kopylov.tm;

import org.jetbrains.annotations.NotNull;
import ru.kopylov.tm.command.project.*;
import ru.kopylov.tm.command.system.AboutCommand;
import ru.kopylov.tm.command.system.ExitCommand;
import ru.kopylov.tm.command.system.HelpCommand;
import ru.kopylov.tm.command.task.*;
import ru.kopylov.tm.command.user.*;
import ru.kopylov.tm.context.Bootstrap;

/**
 * @author Mikhail Kopylov
 * task/project manager
 */

public final class Application {

    @NotNull private static final Class[] CLASSES = {
            ProjectClearCommand.class, ProjectCreateCommand.class,
            ProjectListCommand.class, ProjectRemoveCommand.class,
            ProjectSetTaskCommand.class, ProjectTasksListCommand.class,
            ProjectUpdateCommand.class, TaskClearCommand.class,
            TaskCreateCommand.class, TaskListCommand.class,
            TaskRemoveCommand.class, TaskUpdateCommand.class,
            UserLoginCommand.class, UserLogoutCommand.class,
            UserMyProfileCommand.class, UserRegistrationCommand.class,
            UserUpdateCommand.class, AboutCommand.class,
            ExitCommand.class, HelpCommand.class
    };

    public static void main(String[] args) {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.init(CLASSES);
    }

}
