package ru.kopylov.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.api.repository.ITaskOwnerRepository;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@NoArgsConstructor
public final class TaskOwnerRepository implements ITaskOwnerRepository {

    @NotNull
    private final static Map<String, List<String>> tasksOwner = new LinkedHashMap<>();

    public void merge(@NotNull final String projectId, @NotNull final String taskId) {
        if (tasksOwner.containsKey(projectId)) tasksOwner.get(projectId).add(taskId);
        else {
            @NotNull final List<String> taskList = new ArrayList<>();
            taskList.add(taskId);
            tasksOwner.put(projectId, taskList);
        }
    }

    @NotNull
    public List<String> findAllByProjectId(@NotNull final String projectId) {
        return tasksOwner.get(projectId);
    }

    @NotNull
    public List<String> findAll() { //возвращает список задач, принадлежащих всем проектам
        @NotNull final List<String> allTaskIdList = new ArrayList<>();
        for (@NotNull final Map.Entry<String, List<String>> entry : tasksOwner.entrySet()) {
            allTaskIdList.addAll(entry.getValue());
        }
        return allTaskIdList;
    }

}
