package ru.kopylov.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.api.repository.ITaskRepository;
import ru.kopylov.tm.entity.Task;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@NoArgsConstructor
public final class TaskRepository extends AbstractRepository implements ITaskRepository {

    @NotNull
    private final static Map<String, Task> taskMap = new LinkedHashMap<>();

    public void merge(@NotNull final Task task) {
        taskMap.put(task.getId(), task);
    }

    @Nullable
    public Task persist(@NotNull final Task task) {
        return taskMap.putIfAbsent(task.getId(), task);
    }

    @NotNull
    public List<Task> findAll() {
        return new ArrayList<>(taskMap.values());
    }

    @NotNull
    public List<Task> findAll(@NotNull final String currentUserId) {
        @NotNull final List<Task> tasks = new ArrayList<>();
        for (@NotNull final Map.Entry<String, Task> entry : taskMap.entrySet()) {
            if (currentUserId.equals(entry.getValue().getUserId()))
                tasks.add(entry.getValue());
        }
        return tasks;
    }

    public boolean remove(@NotNull final String taskId) {
        return taskMap.entrySet()
                .removeIf(entry -> taskId.equals(entry.getKey()));
    }

    public void removeAll() {
        taskMap.clear();
    }

    public void removeAll(@NotNull final String currentUserId) {
        taskMap.entrySet()
                .removeIf(entry -> currentUserId.equals(entry.getValue().getUserId()));
    }

    @Nullable
    public Task findOne(@NotNull final String taskId) {
        return taskMap.get(taskId);
    }

    @NotNull
    public List<Task> findAllById(@NotNull final List<String> taskIdList) {
        @NotNull final List<Task> taskNameList = new ArrayList<>();
        for (@NotNull final String taskId : taskIdList) {
            taskNameList.add(TaskRepository.taskMap.get(taskId));
        }
        return taskNameList;
    }

}
