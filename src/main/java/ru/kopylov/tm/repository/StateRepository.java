package ru.kopylov.tm.repository;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.kopylov.tm.api.repository.IStateRepository;
import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.entity.User;

import java.util.LinkedHashMap;
import java.util.Map;

@Getter
@Setter
@NoArgsConstructor
public final class StateRepository extends AbstractRepository implements IStateRepository {

    @NotNull
    private User currentUser = new User();

    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    public void registry(@NotNull final AbstractCommand command) {
        commands.put(command.getName(), command);
    }

    @Override
    public boolean remove(@NotNull final String commandName) {
        return commands.entrySet()
                .removeIf(entry -> commandName.equals(entry.getValue().getName()));
    }

}
