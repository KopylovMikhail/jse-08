package ru.kopylov.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.api.repository.IProjectRepository;
import ru.kopylov.tm.entity.Project;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@NoArgsConstructor
public final class ProjectRepository extends AbstractRepository implements IProjectRepository {

    @NotNull
    private final static Map<String, Project> projectMap = new LinkedHashMap<>();

    public void merge(@NotNull final Project project) {
        projectMap.put(project.getId(), project);
    }

    @Nullable
    public Project persist(@NotNull final Project project) {
        return projectMap.putIfAbsent(project.getId(), project);
    }

    @NotNull
    public List<Project> findAll() {
        return new ArrayList<>(projectMap.values());
    }

    @NotNull
    public List<Project> findAll(@NotNull final String currentUserId) {
        @NotNull final List<Project> projects = new ArrayList<>();
        for (@NotNull final Map.Entry<String, Project> entry : projectMap.entrySet()) {
            if (currentUserId.equals(entry.getValue().getUserId()))
                projects.add(entry.getValue());
        }
        return projects;
    }

    public boolean remove(@NotNull final String projectId) {
        return projectMap.entrySet()
                .removeIf(entry -> projectId.equals(entry.getKey()));
    }

    public void removeAll() {
        projectMap.clear();
    }

    public void removeAll(@NotNull final String currentUserId) {
        projectMap.entrySet()
                .removeIf(entry -> currentUserId.equals(entry.getValue().getUserId()));
    }

    @Nullable
    public Project findOne(@NotNull final String projectId) {
        return projectMap.get(projectId);
    }

    @Nullable
    public Project findOne(@NotNull final String projectId, @NotNull final String currentUserId) {
        for (@NotNull final Map.Entry<String, Project> entry : projectMap.entrySet()) {
            if (projectId.equals(entry.getKey()) & currentUserId.equals(entry.getValue().getUserId())) {
                return entry.getValue();
            }
        }
        return null;
    }

}
