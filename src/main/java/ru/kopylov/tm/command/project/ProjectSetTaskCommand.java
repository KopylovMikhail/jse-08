package ru.kopylov.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.entity.Project;
import ru.kopylov.tm.entity.Task;
import ru.kopylov.tm.util.CommandUtil;

import java.io.IOException;
import java.util.List;

@NoArgsConstructor
public final class ProjectSetTaskCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-set-task";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Assign a task to a project.";
    }

    @Override
    public void execute() {
        System.out.println("[SET PROJECT'S TASK]\n" +
                "ENTER EXISTING PROJECT NUMBER:");
        try {
            @Nullable final String currentUserId = bootstrap.getStateService().getCurrentUser().getId();
            @NotNull final List<Project> projectList = bootstrap.getProjectService().findAll(currentUserId);
            CommandUtil.printProjectList(projectList);
            final int projectNumber = Integer.parseInt(bootstrap.getTerminalService().getReadLine());
            @NotNull final Project project = projectList.get(projectNumber - 1);
            System.out.println("ENTER EXISTING TASK NUMBER:");
            @NotNull final List<Task> taskList = bootstrap.getTaskService().findAll(currentUserId);
            CommandUtil.printTaskList(taskList);
            final int taskNumber = Integer.parseInt(bootstrap.getTerminalService().getReadLine());
            @NotNull final Task task = taskList.get(taskNumber - 1);
            final boolean setSuccess = bootstrap.getProjectService().setTask(project.getId(), task.getId(), currentUserId);
            if (setSuccess)
                System.out.println("[OK]\n");
            else System.out.println("SUCH A PROJECT/TASK DOES NOT EXIST OR NAME IS EMPTY.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
