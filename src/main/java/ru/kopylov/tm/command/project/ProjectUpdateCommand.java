package ru.kopylov.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.entity.Project;
import ru.kopylov.tm.util.CommandUtil;
import ru.kopylov.tm.util.DateUtil;

import java.io.IOException;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

@NoArgsConstructor
public final class ProjectUpdateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-update";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Update selected project.";
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT UPDATE]\n" +
                "ENTER EXISTING PROJECT NUMBER:");
        try {
            @Nullable final String currentUserId = bootstrap.getStateService().getCurrentUser().getId();
            @NotNull final List<Project> projectList = bootstrap.getProjectService().findAll(currentUserId);
            CommandUtil.printProjectList(projectList);
            final int projectNumber = Integer.parseInt(bootstrap.getTerminalService().getReadLine());
            @NotNull final Project project = projectList.get(projectNumber - 1);
            System.out.println("ENTER NEW PROJECT NAME:");
            project.setName(bootstrap.getTerminalService().getReadLine());
            System.out.println("ENTER PROJECT DESCRIPTION:");
            project.setDescription(bootstrap.getTerminalService().getReadLine());
            System.out.println("ENTER PROJECT DATE START (DD.MM.YYYY):");
            @NotNull final Date dateStart = DateUtil.stringToDate(bootstrap.getTerminalService().getReadLine());
            project.setDateStart(dateStart);
            System.out.println("ENTER PROJECT DATE FINISH (DD.MM.YYYY):");
            @NotNull final Date dateFinish = DateUtil.stringToDate(bootstrap.getTerminalService().getReadLine());
            project.setDateFinish(dateFinish);
            if (bootstrap.getProjectService().merge(project))
                System.out.println("[PROJECT HAS BEEN UPDATED]\n");
            else System.out.println("SUCH A PROJECT DOES NOT EXIST OR NAME IS EMPTY.\n");
        }
        catch (IOException | ParseException e) {
            e.printStackTrace();
        }
    }

}
