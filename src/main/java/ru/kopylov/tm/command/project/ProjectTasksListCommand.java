package ru.kopylov.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.entity.Project;
import ru.kopylov.tm.util.CommandUtil;

import java.io.IOException;
import java.util.List;

@NoArgsConstructor
public final class ProjectTasksListCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-tasks-list";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Project task list.";
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT'S TASK LIST]\n" +
                "ENTER EXISTING PROJECT NUMBER:");
        try {
            @Nullable final String currentUserId = bootstrap.getStateService().getCurrentUser().getId();
            @NotNull final List<Project> projectList = bootstrap.getProjectService().findAll(currentUserId);
            CommandUtil.printProjectList(projectList);
            final int projectNumber = Integer.parseInt(bootstrap.getTerminalService().getReadLine());
            @NotNull final Project project = projectList.get(projectNumber - 1);
            @NotNull final List<String> taskList = bootstrap.getProjectService().tasksList(project.getId(), currentUserId);
            System.out.println("TASKS LIST FOR PROJECT " + project.getName() + ":");
            int count = 1;
            for (@NotNull final String taskName : taskList) {
                System.out.println(count++ + ". " + taskName);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.print("\n");
    }

}
