package ru.kopylov.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.entity.Project;

import java.io.IOException;
import java.util.Date;

@NoArgsConstructor
public final class ProjectCreateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return  "project-create";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Create new project.";
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT CREATE]\n" +
                "ENTER NAME:");
        try {
            @Nullable final String projectName = bootstrap.getTerminalService().getReadLine();
            @Nullable final String currentUserId = bootstrap.getStateService().getCurrentUser().getId();
            @NotNull final Project project = new Project();
            project.setName(projectName);
            project.setUserId(currentUserId);
            project.setDateStart(new Date());
            project.setDateFinish(new Date());
            final boolean createSuccess = bootstrap.getProjectService().persist(project);
            if (createSuccess) System.out.println("[OK]\n");
            else System.out.println("[SUCH A PROJECT EXISTS OR NAME IS EMPTY.]\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
