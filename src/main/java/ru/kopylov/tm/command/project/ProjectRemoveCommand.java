package ru.kopylov.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.entity.Project;
import ru.kopylov.tm.util.CommandUtil;

import java.io.IOException;
import java.util.List;

@NoArgsConstructor
public final class ProjectRemoveCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-remove";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove selected project.";
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT REMOVE]\n" +
                "ENTER EXISTING PROJECT NUMBER:");
        try {
            @Nullable final String currentUserId = bootstrap.getStateService().getCurrentUser().getId();
            @NotNull final List<Project> projectList = bootstrap.getProjectService().findAll(currentUserId);
            CommandUtil.printProjectList(projectList);
            final int projectNumber = Integer.parseInt(bootstrap.getTerminalService().getReadLine());
            @NotNull final Project project = projectList.get(projectNumber - 1);
            final boolean removeSuccess = bootstrap.getProjectService().remove(project.getId());
            if (removeSuccess)
                System.out.println("[PROJECT " + projectNumber + ". " + project.getName() + " REMOVED]\n");
            else System.out.println("SUCH A PROJECT DOES NOT EXIST OR NAME IS EMPTY.\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
