package ru.kopylov.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.entity.User;
import ru.kopylov.tm.util.HashUtil;

import java.io.IOException;
import java.util.List;

@NoArgsConstructor
public final class UserLoginCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "login";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "User authorization.";
    }

    @Override
    public void execute() {
        System.out.println("[USER AUTHORIZATION]\n" +
                "ENTER LOGIN:");
        try {
            @Nullable final String login = bootstrap.getTerminalService().getReadLine();
            System.out.println("ENTER PASSWORD:");
            @Nullable final String password = bootstrap.getTerminalService().getReadLine();
            if (password == null || password.isEmpty() || login == null || login.isEmpty()) {
                System.out.println("LOGIN OR PASSWORD IS EMPTY.\n");
                return;
            }
            @NotNull final String hashPassword = HashUtil.hash(password);
            @Nullable User loggedUser = null;
            @NotNull final List<User> userList = bootstrap.getUserService().findAll();
            for (@NotNull User user : userList) {
                if (login.equals(user.getLogin()) && hashPassword.equals(user.getPassword())) {
                    loggedUser = user;
                    break;
                }
            }
            if (loggedUser == null) {
                System.out.println("SUCH USER DOES NOT EXIST OR LOGIN/PASSWORD IS INCORRECT.\n");
                return;
            }
            bootstrap.getStateService().setCurrentUser(loggedUser);
            System.out.println("[AUTHORIZATION SUCCESS]\n" + "HELLO, " + login + "!\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
