package ru.kopylov.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.entity.User;
import ru.kopylov.tm.util.HashUtil;

import java.io.IOException;

@NoArgsConstructor
public final class UserUpdateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "user-update";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Update user password.";
    }

    @Override
    public void execute() {
        System.out.println("[USER PASSWORD UPDATE]\n" +
                "ENTER NEW LOGIN:");
        try {
            @Nullable final String newLogin = bootstrap.getTerminalService().getReadLine();
            System.out.println("ENTER NEW PASSWORD:");
            @Nullable final String newPassword = bootstrap.getTerminalService().getReadLine();
            if (newLogin == null || newLogin.isEmpty() || newPassword == null || newPassword.isEmpty()) {
                System.out.println("LOGIN OR PASSWORD IS EMPTY.\n");
                return;
            }
            @NotNull final String newHashPassword = HashUtil.hash(newPassword);
            @NotNull final User currentUser = bootstrap.getStateService().getCurrentUser();
            currentUser.setLogin(newLogin);
            currentUser.setPassword(newHashPassword);
            final boolean updateSuccess = bootstrap.getUserService().merge(currentUser);
            if (!updateSuccess) {
                System.out.println("PASSWORD OR LOGIN IS EMPTY.\n");
                return;
            }
            bootstrap.getStateService().setCurrentUser(currentUser);
            System.out.println("[PASSWORD UPDATE SUCCESS]\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
