package ru.kopylov.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.kopylov.tm.command.AbstractCommand;

@NoArgsConstructor
public final class UserMyProfileCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "my-profile";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show current user profile.";
    }

    @Override
    public void execute() {
        if (bootstrap.getStateService().getCurrentUser() == null || bootstrap.getStateService().getCurrentUser().getRole() == null) {
            System.out.println("YOU NEED LOGIN.\n");
            return;
        }
        System.out.println("[USER PROFILE]\n" +
                "LOGIN: " + bootstrap.getStateService().getCurrentUser().getLogin() + "\n" +
                "ROLE: " + bootstrap.getStateService().getCurrentUser().getRole().getDisplayName() + "\n");
    }

}
