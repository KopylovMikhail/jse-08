package ru.kopylov.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.entity.User;

@NoArgsConstructor
public final class UserLogoutCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "logout";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Complete user session.";
    }

    @Override
    public void execute() {
        bootstrap.getStateService().setCurrentUser(new User());
        System.out.println("[LOGOUT SUCCESS]\n");
    }

}
