package ru.kopylov.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.entity.User;
import ru.kopylov.tm.enumerated.TypeRole;
import ru.kopylov.tm.util.HashUtil;

import java.io.IOException;

@NoArgsConstructor
public final class UserRegistrationCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "registration";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "User registration.";
    }

    @Override
    public void execute() {
        System.out.println("[USER REGISTRATION]\n" +
                "ENTER LOGIN:");
        try {
            @Nullable final String login = bootstrap.getTerminalService().getReadLine();
            System.out.println("ENTER PASSWORD:");
            @Nullable final String password = bootstrap.getTerminalService().getReadLine();
            if (password == null || password.isEmpty() || login == null || login.isEmpty()) {
                System.out.println("LOGIN OR PASSWORD IS EMPTY.\n");
                return;
            }
            @NotNull final String hashPassword = HashUtil.hash(password);
            @NotNull final User user = new User();
            user.setLogin(login);
            user.setPassword(hashPassword);
            user.setRole(TypeRole.USER);
            final boolean registrationSuccess = bootstrap.getUserService().persist(user);
            if (!registrationSuccess) {
                System.out.println("SUCH USER ALREADY EXIST OR LOGIN/PASSWORD IS EMPTY.\n");
                return;
            }
            bootstrap.getStateService().setCurrentUser(user);
            System.out.println("[REGISTRATION SUCCESS]\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
