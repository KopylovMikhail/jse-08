package ru.kopylov.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.command.AbstractCommand;

@NoArgsConstructor
public final class TaskClearCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-clear";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove all tasks.";
    }

    @Override
    public void execute() {
        @Nullable final String currentUserId = bootstrap.getStateService().getCurrentUser().getId();
        bootstrap.getTaskService().removeAll(currentUserId);
        System.out.println("[ALL TASKS REMOVED]\n");
    }

}
