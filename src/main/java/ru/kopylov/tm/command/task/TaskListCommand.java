package ru.kopylov.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.entity.Task;
import ru.kopylov.tm.util.CommandUtil;

import java.util.List;

@NoArgsConstructor
public final class TaskListCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-list";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show all tasks.";
    }

    @Override
    public void execute() {
        @Nullable final String currentUserId = bootstrap.getStateService().getCurrentUser().getId();
        @NotNull final List<Task> taskList = bootstrap.getTaskService().findAll(currentUserId);
        CommandUtil.printTaskListWithParam(taskList);
    }

}
