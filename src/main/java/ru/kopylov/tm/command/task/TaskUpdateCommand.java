package ru.kopylov.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.entity.Task;
import ru.kopylov.tm.util.CommandUtil;
import ru.kopylov.tm.util.DateUtil;

import java.io.IOException;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

@NoArgsConstructor
public final class TaskUpdateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-update";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Update selected task.";
    }

    @Override
    public void execute() {
        System.out.println("[TASK UPDATE]\n" +
                "ENTER EXISTING TASK NAME:");
        try {
            @Nullable final String currentUserId = bootstrap.getStateService().getCurrentUser().getId();
            @NotNull final List<Task> taskList = bootstrap.getTaskService().findAll(currentUserId);
            CommandUtil.printTaskList(taskList);
            final int taskNumber = Integer.parseInt(bootstrap.getTerminalService().getReadLine());
            @NotNull final Task task = taskList.get(taskNumber - 1);
            System.out.println("ENTER NEW TASK NAME:");
            task.setName(bootstrap.getTerminalService().getReadLine());
            System.out.println("ENTER TASK DESCRIPTION:");
            task.setDescription(bootstrap.getTerminalService().getReadLine());
            System.out.println("ENTER PROJECT DATE START (DD.MM.YYYY):");
            @NotNull final Date dateStart = DateUtil.stringToDate(bootstrap.getTerminalService().getReadLine());
            task.setDateStart(dateStart);
            System.out.println("ENTER PROJECT DATE FINISH (DD.MM.YYYY):");
            @NotNull final Date dateFinish = DateUtil.stringToDate(bootstrap.getTerminalService().getReadLine());
            task.setDateFinish(dateFinish);
            final boolean updateSuccess = bootstrap.getTaskService().merge(task);
            if (updateSuccess) System.out.println("[TASK HAS BEEN UPDATED]\n");
            else System.out.println("SUCH A TASK DOES NOT EXIST OR NAME IS EMPTY.\n");
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
    }

}
