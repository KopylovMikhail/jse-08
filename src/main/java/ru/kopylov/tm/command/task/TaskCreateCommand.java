package ru.kopylov.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.entity.Task;

import java.io.IOException;
import java.util.Date;

@NoArgsConstructor
public final class TaskCreateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-create";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Create new task.";
    }

    @Override
    public void execute() {
        System.out.println("[TASK CREATE]\n" +
                "ENTER NAME:");
        try {
            @Nullable final String taskName = bootstrap.getTerminalService().getReadLine();
            @Nullable final String currentUserId = bootstrap.getStateService().getCurrentUser().getId();
            @NotNull final Task task = new Task();
            task.setName(taskName);
            task.setUserId(currentUserId);
            task.setDateStart(new Date());
            task.setDateFinish(new Date());
            final boolean createSuccess = bootstrap.getTaskService().persist(task);
            if (createSuccess) System.out.println("[OK]\n");
            else System.out.println("[SUCH A TASK EXISTS OR NAME IS EMPTY.]\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
