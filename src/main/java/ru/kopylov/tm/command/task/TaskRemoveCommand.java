package ru.kopylov.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.entity.Task;
import ru.kopylov.tm.util.CommandUtil;

import java.io.IOException;
import java.util.List;

@NoArgsConstructor
public final class TaskRemoveCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-remove";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove selected task.";
    }

    @Override
    public void execute() {
        System.out.println("[TASK REMOVE]\n" +
                "ENTER EXISTING TASK NUMBER:");
        try {
            @Nullable final String currentUserId = bootstrap.getStateService().getCurrentUser().getId();
            @NotNull final List<Task> taskList = bootstrap.getTaskService().findAll(currentUserId);
            CommandUtil.printTaskList(taskList);
            final int taskNumber = Integer.parseInt(bootstrap.getTerminalService().getReadLine());
            @NotNull final Task task = taskList.get(taskNumber - 1);
            final boolean removeSuccess = bootstrap.getTaskService().remove(task.getId());
            if (removeSuccess)
                System.out.println("[TASK " + taskNumber + ". " + task.getName() + " REMOVED]\n");
            else System.out.println("SUCH A TASK DOES NOT EXIST OR NAME IS EMPTY.\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
