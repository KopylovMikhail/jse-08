package ru.kopylov.tm.command.system;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.kopylov.tm.command.AbstractCommand;

@NoArgsConstructor
public final class ExitCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "exit";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Exit from application.";
    }

    @Override
    public void execute() {
        System.exit(0);
    }

}
