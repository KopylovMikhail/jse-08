package ru.kopylov.tm.command.system;

import com.jcabi.manifests.Manifests;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.kopylov.tm.command.AbstractCommand;

@NoArgsConstructor
public final class AboutCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "about";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Information about application.";
    }

    @Override
    public void execute() {
        @NotNull final String version = Manifests.read("Implementation-Version");
        @NotNull final String developer = Manifests.read("Built-By");
        System.out.println("[ABOUT]"
                    + "\nTask manager"
                    + "\nversion: " + version
                    + "\ndeveloper: " + developer + "\n");
    }

}
