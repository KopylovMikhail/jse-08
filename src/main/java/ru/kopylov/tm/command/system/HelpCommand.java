package ru.kopylov.tm.command.system;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.kopylov.tm.command.AbstractCommand;

import java.util.Map;

@NoArgsConstructor
public final class HelpCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "help";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show all commands.";
    }

    @Override
    public void execute() {
        for (@NotNull final Map.Entry<String, AbstractCommand> entry : bootstrap.getStateService().getCommands().entrySet()) {
            System.out.println(entry.getKey() + ": " + entry.getValue().getDescription());
        }
        System.out.print("\n");
    }

}
