package ru.kopylov.tm.command;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.kopylov.tm.api.service.ServiceLocator;
import ru.kopylov.tm.context.Bootstrap;

@NoArgsConstructor
public abstract class AbstractCommand {

    @NotNull
    protected ServiceLocator bootstrap = new Bootstrap();

    public AbstractCommand(@NotNull final ServiceLocator bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void setBootstrap(@NotNull final ServiceLocator bootstrap) {
        this.bootstrap = bootstrap;
    }

    @NotNull
    public abstract String getName();

    @NotNull
    public abstract String getDescription();

    public abstract void execute();

}
