package ru.kopylov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.entity.Project;

import java.util.List;

public interface IProjectRepository {

    void merge(@NotNull Project project);

    Project persist(@NotNull Project project);

    @NotNull
    List<Project> findAll();

    @NotNull
    List<Project> findAll(@NotNull String currentUserId);

    boolean remove(@NotNull String projectId);

    void removeAll();

    void removeAll(@NotNull String currentUserId);

    @Nullable
    Project findOne(@NotNull String projectId);

    @Nullable
    Project findOne(@NotNull String projectId, @NotNull String currentUserId);

}
