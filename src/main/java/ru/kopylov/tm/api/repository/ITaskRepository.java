package ru.kopylov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.entity.Task;

import java.util.List;

public interface ITaskRepository {

    void merge(@NotNull Task task);

    @Nullable
    Task persist(@NotNull Task task);

    @NotNull
    List<Task> findAll();

    @NotNull
    List<Task> findAll(@NotNull String currentUserId);

    boolean remove(@NotNull String taskId);

    void removeAll();

    void removeAll(@NotNull String currentUserId);

    @Nullable
    Task findOne(@NotNull String taskId);

    @NotNull
    List<Task> findAllById(@NotNull List<String> taskIdList);

}
