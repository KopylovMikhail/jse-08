package ru.kopylov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.entity.User;

import java.util.List;

public interface IUserRepository {

    void merge(@NotNull User user);

    @Nullable
    User persist(@NotNull User user);

    @Nullable
    User findOne(@NotNull String id);

    boolean remove(@NotNull String id);

    @NotNull
    List<User> findAll();

}
