package ru.kopylov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.entity.Project;

import java.util.List;

public interface IProjectService {

    boolean persist(@Nullable Project project);

    @NotNull
    List<Project> findAll();

    @NotNull
    List<Project> findAll(String currentUserId);

    boolean merge(@Nullable Project project);

    boolean remove(@Nullable String projectId);

    void removeAll();

    void removeAll(@Nullable String currentUserId);

    boolean setTask(@Nullable String projectId, @Nullable String taskId, @Nullable String currentUserId);

    @NotNull
    List<String> tasksList(@Nullable String projectName, @Nullable String currentUserId);

}
