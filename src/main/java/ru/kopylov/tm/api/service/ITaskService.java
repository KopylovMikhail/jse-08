package ru.kopylov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.entity.Task;

import java.util.List;

public interface ITaskService {

    boolean persist(@Nullable Task task);

    @NotNull
    List<Task> findAll();

    @NotNull
    List<Task> findAll(@Nullable String currentUserId);

    boolean merge(@Nullable Task task);

    boolean remove(@Nullable String taskId);

    void removeAll();

    void removeAll(@Nullable String currentUserId);

}
