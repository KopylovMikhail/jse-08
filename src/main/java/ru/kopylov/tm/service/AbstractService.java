package ru.kopylov.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.entity.AbstractEntity;
import ru.kopylov.tm.repository.AbstractRepository;

import java.util.List;

@NoArgsConstructor
public abstract class AbstractService<T extends AbstractEntity> {

    protected AbstractRepository<T> abstractRepository;

    public AbstractService(@NotNull AbstractRepository<T> abstractRepository) {
        this.abstractRepository = abstractRepository;
    }

    @NotNull
    public List<T> findAll() {
        return abstractRepository.findAll();
    }
}
