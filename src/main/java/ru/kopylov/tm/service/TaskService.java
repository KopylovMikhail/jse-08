package ru.kopylov.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.api.service.ITaskService;
import ru.kopylov.tm.entity.Task;
import ru.kopylov.tm.api.repository.ITaskRepository;

import java.util.Collections;
import java.util.List;

@NoArgsConstructor
public final class TaskService extends AbstractService implements ITaskService {

    @NotNull
    private ITaskRepository taskRepository = (ITaskRepository) abstractRepository;

    public TaskService(@NotNull final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public boolean persist(@Nullable final Task task) {
        if (task == null ) return false;
        return !task.equals(taskRepository.persist(task));
    }

    @NotNull
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @NotNull
    public List<Task> findAll(final String currentUserId) {
        if (currentUserId == null || currentUserId.isEmpty()) return Collections.emptyList();
        return taskRepository.findAll(currentUserId);
    }

    public boolean merge(@Nullable final Task task) {
        if (task == null ) return false;
        if (task.getId() == null || task.getId().isEmpty()) return false;
        if (taskRepository.findOne(task.getId()) == null) return false;
        taskRepository.merge(task);
        return true;
    }

    public boolean remove(@Nullable final String taskId) {
        if (taskId == null || taskId.isEmpty()) return false;
        return taskRepository.remove(taskId);
    }

    public void removeAll() {
        taskRepository.removeAll();
    }

    public void removeAll(@Nullable final String currentUserId) {
        if (currentUserId == null || currentUserId.isEmpty()) return;
        taskRepository.removeAll(currentUserId);
    }

}
