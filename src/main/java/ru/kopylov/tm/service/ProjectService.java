package ru.kopylov.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.api.repository.IProjectRepository;
import ru.kopylov.tm.api.repository.ITaskOwnerRepository;
import ru.kopylov.tm.api.repository.ITaskRepository;
import ru.kopylov.tm.api.service.IProjectService;
import ru.kopylov.tm.entity.Project;
import ru.kopylov.tm.entity.Task;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@NoArgsConstructor
public final class ProjectService extends AbstractService implements IProjectService {

    @NotNull
    private IProjectRepository projectRepository = (IProjectRepository) abstractRepository;

    @NotNull
    private ITaskRepository taskRepository = (ITaskRepository) abstractRepository;

    @NotNull
    private ITaskOwnerRepository taskOwnerRepository = (ITaskOwnerRepository) abstractRepository;

    public ProjectService(
            @NotNull final IProjectRepository projectRepository,
            @NotNull final ITaskRepository taskRepository,
            @NotNull final ITaskOwnerRepository taskOwnerRepository
    ) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
        this.taskOwnerRepository = taskOwnerRepository;
    }

    public boolean persist(@Nullable final Project project) {
        if (project == null) return false;
        return !project.equals(projectRepository.persist(project));
    }

    @NotNull
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @NotNull
    public List<Project> findAll(@Nullable final String currentUserId) {
        if (currentUserId == null || currentUserId.isEmpty()) return Collections.emptyList();
        return projectRepository.findAll(currentUserId);
    }

    public boolean merge(@Nullable final Project project) {
        if (project == null) return false;
        if (project.getId() == null || project.getId().isEmpty()) return false;
        if (projectRepository.findOne(project.getId()) == null) return false;
        projectRepository.merge(project);
        return true;
    }

    public boolean remove(@Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) return false;
        @Nullable final List<String> taskIdList = taskOwnerRepository.findAllByProjectId(projectId);
        for (@NotNull final String taskId : taskIdList) {
            taskRepository.remove(taskId); //вместе с удалением проекта удаляем все задачи проекта
        }
        return projectRepository.remove(projectId);
    }

    public void removeAll() {
        @NotNull final List<String> allTaskIdList = taskOwnerRepository.findAll();
        for (@NotNull final String taskId : allTaskIdList) {
            taskRepository.remove(taskId); //удаляя все проекты, удаляем все задачи проектов
        }
        projectRepository.removeAll();
    }

    public void removeAll(@Nullable final String currentUserId) {
        if (currentUserId == null || currentUserId.isEmpty()) return;
        @NotNull final List<String> allTaskIdList = new ArrayList<>();
        @NotNull final List<Project> projects = projectRepository.findAll(currentUserId);
        for (@NotNull final Project project : projects) {
            if (project.getId() == null || project.getId().isEmpty()) continue;
            taskOwnerRepository.findAllByProjectId(project.getId());
            allTaskIdList.addAll(taskOwnerRepository.findAllByProjectId(project.getId()));
        }
        for (@NotNull final String taskId : allTaskIdList) {
            taskRepository.remove(taskId); //удаляя все проекты, удаляем все задачи проектов
        }
        projectRepository.removeAll(currentUserId);
    }

    public boolean setTask(
            @Nullable final String projectId,
            @Nullable final String taskId,
            @Nullable final String currentUserId
    ) {
        if (projectId == null || projectId.isEmpty()) return false;
        if (taskId == null || taskId.isEmpty()) return false;
        if (currentUserId == null || currentUserId.isEmpty()) return false;
        taskOwnerRepository.merge(projectId, taskId);
        return true;
    }

    @NotNull
    public List<String> tasksList(@Nullable final String projectId, @Nullable final String currentUserId) { //возвращает список задач проекта
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        if (currentUserId == null || currentUserId.isEmpty()) return Collections.emptyList();
        if (projectRepository.findOne(projectId, currentUserId) == null) return Collections.emptyList();
        taskOwnerRepository.findAllByProjectId(projectId);
        @NotNull final List<String> taskIdList = taskOwnerRepository.findAllByProjectId(projectId);
        taskRepository.findAllById(taskIdList);
        @NotNull final List<Task> taskList = taskRepository.findAllById(taskIdList);
        @NotNull final List<String> taskNameList = new ArrayList<>();
        for (@NotNull final Task task : taskList) {
            taskNameList.add(task.getName());
        }
        return taskNameList;
    }

}
