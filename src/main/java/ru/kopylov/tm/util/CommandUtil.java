package ru.kopylov.tm.util;

import org.jetbrains.annotations.NotNull;
import ru.kopylov.tm.entity.Project;
import ru.kopylov.tm.entity.Task;

import java.util.List;

public final class CommandUtil {

    public static void printProjectList(@NotNull List<Project> projectList) {
        System.out.println("[PROJECT LIST]");
        int count = 1;
        for (@NotNull final Project project : projectList) {
            System.out.println(count++ + ". " + project.getName());
        }
    }

    public static void printTaskList(@NotNull List<Task> taskList) {
        System.out.println("[TASK LIST]");
        int count = 1;
        for (final Task task : taskList) {
            System.out.println(count++ + ". " + task.getName());
        }
    }

    public static void printProjectListWithParam(@NotNull List<Project> projectList) {
        System.out.println("[PROJECT LIST]");
        int count = 1;
        for (@NotNull final Project project : projectList) {
            System.out.println(count++ + ". " + project.getName() +
                    "; description: " + project.getDescription() +
                    "; date start: " + DateUtil.dateToString(project.getDateStart()) +
                    "; date finish: " + DateUtil.dateToString(project.getDateFinish()));
        }
    }

    public static void printTaskListWithParam(@NotNull List<Task> taskList) {
        System.out.println("[TASK LIST]");
        int count = 1;
        for (final Task task : taskList) {
            System.out.println(count++ + ". " + task.getName() +
                    "; description: " + task.getDescription() +
                    "; date start: " + DateUtil.dateToString(task.getDateStart()) +
                    "; date finish: " + DateUtil.dateToString(task.getDateFinish()));
        }
    }

}
